﻿using UnityEngine;
using System.Collections;

public class Spectrum01 : MonoBehaviour {

	public GameObject prefab;
	public int numObj = 10;
	public float rad;
	public GameObject[] c;
	GameObject allPrefabs;
	public float intensity;
	AudioSource audio;
	public float height;
	
	// Use this for initialization
	void Start () {
		for (int i = 0; i < numObj; i++) {
			float angle = i * Mathf.PI * 2 / numObj;
			Vector3 pos = new Vector3 (rad*Mathf.Cos (angle), 0, rad*Mathf.Sin (angle));
			allPrefabs = Instantiate (prefab, pos, Quaternion.identity) as GameObject;
			allPrefabs.transform.parent = this.transform;
			audio = GetComponent<AudioSource>();
			allPrefabs.transform.position += Vector3.up * height * Time.deltaTime;

		}
		c = GameObject.FindGameObjectsWithTag ("cubes01");
	}
	
	// Update is called once per frame
	void Update () {

		float[] spectrum = audio.GetSpectrumData (1024, 0, FFTWindow.Hamming);
		for (int i = 0; i < numObj; i++) {
			Vector3 previousScale = c[i].transform.localScale;
			previousScale.y = Mathf.Lerp (previousScale.y, spectrum [i] * intensity, Time.deltaTime * 30);
			c[i].transform.localScale = previousScale;
		}
	}
}
